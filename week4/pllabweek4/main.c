#include <stdio.h>
#include <stdlib.h>
#include <math.h>
FILE *outp;
float arb_func(float x){
  return x*log10(x)-1.2;
}

void rf(float *x,float x0,float x1,float fx0,float fx1,int *itr){

    *x = ((x0*fx1)-(x1*fx1)) / (fx1-fx0);
    ++(*itr);
    printf("Iteration %d: %.5f\n",*itr,*x);
    fprintf(outp,"Iteration %d: %.5f\n",*itr,*x);
}

void main()
{
    outp =  fopen("rf.txt","w");
    int itr=0; // num of iterations
    int maxitr; // num of allowed iterations
    float x0; // interval start point
    float x1; // interval end point
    float x_curr; // currently computed root
    float x_next; // root in next step
    float error; //  a very small number which is the maximum allowed error

    printf("Enter interval values [x0,x1], allowed error, and number of iterations:\n");
    scanf("%lf %lf %lf %d",&x0,&x1,&error,&maxitr);
    rf(&x_curr,x0,x1,arb_func(x0),arb_func(x1),&itr);
    do{
        if((arb_func(x0)*arb_func(x_curr))<0){
       // printf("If'e girdi\n");
            x1=x_curr;
        }
        else{
           // printf("Else'e girdi\n");
            x0=x_curr;
        }

        rf(&x_next,x0,x1,arb_func(x0),arb_func(x1),&itr);
        if(fabs(x_next-x_curr)<error){
            printf("After %d iterations root is %f\n",itr,x_next);
            fprintf("After %d iterations root is %f\n",itr,x_next);
            return 0;
        }else{
           // printf("Buraya girdi\n");
            x_next=x_curr;
        }

    }while(itr<maxitr);
    printf("Solution doesn't converge or iterations not sufficient.\n");
    fprintf(outp,"Solution doesn't converge or iterations not sufficient.");
    fclose(outp);

    return 0;
}
