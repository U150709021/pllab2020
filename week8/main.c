#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define COMPETITORS 4
#define NUM_OF_SCORES 6
typedef struct {
char name[12],surname[12];
double scores[6];
double average;
}Skater;

double calculateScore(Skater *skater);
double findMax(double arr[]);
double findMin(double arr[]);
void loadSkaters(Skater skaters[],char file_name[]);

int main()
{   char file_name[20];
    printf("Enter the file name to read: ");
    scanf("%s",file_name);
    FILE *input;
    input = fopen("skaters.txt","w");
    fprintf(input,"%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n","Maria","Solevaya",4.0,4.0,5.0,6.0,4.0,5.0);
    fprintf(input,"%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n","July","Son",6.0,5.0,5.0,5.0,6.0,6.0);
    fprintf(input,"%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n","Alexandra","Tatia",5.0,5.0,6.0,6.0,4.0,5.0);
    fprintf(input,"%s %s %.0lf %.0lf %.0lf %.0lf %.0lf %.0lf\n","Joan","Jung",4.0,4.0,5.0,3.0,3.0,4.0);
    fclose(input);

    Skater skaters[COMPETITORS];
    loadSkaters(skaters,file_name);

    return 0;
}

double calculateScore(Skater *skater){
double max,min,sum=0;
int i;
for(i=0;i<NUM_OF_SCORES;i++){
    sum+=(*skater).scores[i];
}
max = findMax((*skater).scores);
min = findMin((*skater).scores);
sum = sum - (max+min);
return sum/8;
}

double findMax(double arr[]){
double max = arr[0];
int i;
for(i=0;i<NUM_OF_SCORES;i++){
    if(max<arr[i]) max=arr[i];
}
return max;

}
double findMin(double arr[NUM_OF_SCORES]){
double min = arr[0];
int i;
for(i=0;i<NUM_OF_SCORES;i++){
    if(min>arr[i]) min=arr[i];
}
return min;

}

void loadSkaters(Skater skaters[],char file_name[]){
double max=0;
int temp;
int i=0,j;
FILE *output;
output=fopen(file_name,"r");
if(output!=NULL){
    while(!feof(output) && i!=4){

        fscanf(output,"%s %s %lf %lf %lf %lf %lf %lf",skaters[i].name,skaters[i].surname,&skaters[i].scores[0],&skaters[i].scores[1],&skaters[i].scores[2],&skaters[i].scores[3],&skaters[i].scores[4],
               &skaters[i].scores[5]);
        skaters[i].average=calculateScore(&skaters[i]);
        printf("**Skater %d:%s %s**\nAverage point of Skater %d is: %.2f\n\n",i+1,skaters[i].name,skaters[i].surname,i+1,skaters[i].average);
        i++;
    }
    printf("Loading Completed.\nFile closed.\n\n");
    fclose(output);
    for(j=0;j<COMPETITORS;j++){
        if(max<skaters[j].average){
                max = skaters[j].average;
                temp = j;
        }
    }
    printf("Winner of the Skating Championships is:\n%s %s with %.2f points in total.\n",skaters[temp].name,skaters[temp].surname,skaters[temp].average);

}
else{
    printf("There is no such a file.");
}
}
